package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

@Configuration
public class WithAutowiredMethod {
    private AnotherDependent anotherdependent;
    private Dependent dependent;
    private Log log;


    public WithAutowiredMethod(Dependent dependent, Log log) {
        this.dependent = dependent;
        this.log = log;
        log.getList().add("constructor WithAutowiredMethod");

    }

    @Autowired
    public void initialize(AnotherDependent anotherdependent) {
        this.anotherdependent = anotherdependent;
        log.getList().add("method initialize");
    }

    public AnotherDependent getAnotherdependent() {
        return anotherdependent;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public Log getLog() {
        return log;
    }
}
