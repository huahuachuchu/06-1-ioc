package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleDependent {
    private String name;
    private Admin admin;

    @Bean
    public void creatAdmin() {
        this.admin = new Admin("O_o");
        this.name = admin.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
