package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependent dependent;
    private String value;
    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String value) {
        this.value = value;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public String getValue() {
        return value;
    }
}
