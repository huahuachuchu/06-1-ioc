package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Log {
    private List<String> list;

    public Log() {
        this.list = new ArrayList<>();
    }

    public List<String> getList() {
        return list;
    }

    public void addLog(String log){
        list.add(log);
    }
}
