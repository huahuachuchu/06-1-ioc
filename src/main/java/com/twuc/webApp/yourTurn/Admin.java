package com.twuc.webApp.yourTurn;

public class Admin {
    private String name;

    public Admin(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
