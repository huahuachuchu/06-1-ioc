package com.twuc.webApp.yourTurn;

import com.twuc.webApp.other.OutOfScanningScope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
class IocTest {
    AnnotationConfigApplicationContext context;
    @BeforeEach
    void should_creat_context(){
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");

    }
    // 2.1
    @Test
    void should_not_have_rely(){
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency);
    }

    @Test
    void should_creat_object_have_rely(){
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        Dependent dependent = context.getBean(Dependent.class);
        assertNotNull(withoutDependency);
        assertNotNull(dependent);
    }

    @Test
    void should_beyond_the_scope(){
        assertThrows(RuntimeException.class, () -> {context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_creat_object_use_interface(){
       Interface result =context.getBean(Interface.class);
       assertNotNull(result);
    }

    @Test
    void should_use_get_name_method_get_name(){
        SimpleDependent simpleDependent = context.getBean(SimpleDependent.class);
        assertEquals("O_o", simpleDependent.getName());

    }

    // 2.2
    @Test
    void should_select_constructor_to_call(){
        MultipleConstructor mc = context.getBean(MultipleConstructor.class);
        assertNotNull(mc.getDependent());
    }

    @Test
    void should_select_method_to_call(){
        WithAutowiredMethod withAutowiredMethod=context.getBean(WithAutowiredMethod.class);
        assertEquals(Arrays.asList("constructor WithAutowiredMethod", "method initialize"),withAutowiredMethod.getLog().getList());
    }


}